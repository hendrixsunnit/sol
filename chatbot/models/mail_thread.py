# Copyright (C) 2020 - Hendrix Costa
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html

from odoo import api, models
from odoo.addons.queue_job.job import job
from odoo import exceptions, tools


import requests
import json


class MailThread(models.AbstractModel):
    _inherit = 'mail.thread'

    @job
    def send_bot_message(self, id, question, author):
        """
        """
        url = self.env['ir.config_parameter'].sudo().\
                  get_param('chatbot_service_url') or "http://localhost:7788/"

        intents = self.env['intent'].search([])

        intents_data = \
            [{"label": x.label, "context": x.context} for x in intents]

        json_data = json.dumps(
            {"question": question, "intents": intents_data})

        result = requests.post(url, json=json_data)

        resposta = result.json().get('answer')

        kwargs = {
            'partner_ids': [],
            'channel_ids': [],
            'body': resposta.capitalize(),
            'attachment_ids': [],
            'canned_response_ids': [],
            'subtype': 'mail.mt_comment',
            'author_id': author.id,
        }

        self.sudo().browse(id).message_post(
            message_type='comment', **kwargs)

        return True

    @api.returns('mail.message', lambda value: value.id)
    def message_post(self, **kwargs):

        message = super(MailThread, self).message_post(**kwargs)

        modulo_habilitado = \
            self.env['ir.config_parameter'].sudo().\
                get_param('has_chatbot_ai') or False
        if not modulo_habilitado:
            return message

        chatbot_user_id = self.env['ir.config_parameter'].sudo().\
            get_param('chatbot_user_id') or False
        if not chatbot_user_id:
            return message

        chatbot_patner_id = \
            self.env['res.users'].browse(int(chatbot_user_id)).partner_id or False
        if not chatbot_patner_id:
            return message

        if chatbot_patner_id in self.channel_partner_ids and \
                kwargs.get("author_id") != chatbot_patner_id.id:
            self.with_delay().send_bot_message(
                self.id, tools.html2plaintext(message.body), chatbot_patner_id)

        return message
